import javafx.application.Application
import javafx.scene.control.Dialogs
import javafx.stage.Modality
import javafx.stage.Stage


class Test extends Application{

	static main(args) {
		launch(Test, args)   
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		// TODO Auto-generated method stub
		def stage = new Stage()
		stage.owner = primaryStage
		stage.initModality(Modality.APPLICATION_MODAL)
		Dialogs.showErrorDialog(null, "Ooops, there was an error!", "错误信息", "title");
	}

}
